﻿using System;

namespace comp5002_10011581_assessment01
{
    class Program
    {
            static void Main(string[] args)
        {
            // Welcome Message
            Console.WriteLine("Welcome to Aman Shop");
            // Variables
            var name="";
            double GST = 0.15;
            double item1;
            string yesOrNo;
            double item2;
            double totalPrice = 0;
            double total = 0;
            // User Name
            Console.WriteLine("Write your Name");
            // variable Storage
            name= Console.ReadLine();
            Console.WriteLine("Hi "+ name);
            // Write any two decimal number 
            Console.WriteLine("Write number with two decimal places");
            item1 = double.Parse(Console.ReadLine());
            // User decision
            Console.WriteLine("Really want this number - Yes or No");
            yesOrNo = Console.ReadLine().ToLower();
            // for yes Condition
            if (yesOrNo == "yes")
            {
                Console.WriteLine("Add other Number");
                item2 = double.Parse(Console.ReadLine());
                totalPrice = item1 + item2; 
            }
            // for no Condition
            else if (yesOrNo == "no")
            {
                totalPrice = item1;
                Console.WriteLine($"Total {totalPrice}");
            }
            // total with GST
            total = totalPrice + totalPrice*GST;
            Console.WriteLine($"Total itmes  = {totalPrice}");
            Console.WriteLine($"GST = {totalPrice*GST}");
            Console.WriteLine($"Last total = {total}");
            // message of Thank You 
            Console.WriteLine("~~ Thank you for shopping with us, please come again ~~");   
        }
    }
}